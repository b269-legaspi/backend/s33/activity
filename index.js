fetch("https://jsonplaceholder.typicode.com/todos")
  // Use the "json()" method from the "response object" to convert the data retrieved into JSON format to be used in our application
  .then((response) => response.json())
  // Print the converted JSON value from the "fetch" request
  .then((json) => console.log(json.map((data) => data.title)));

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  // Print the converted JSON value from the "fetch" request
  .then((json) =>
    console.log(`title:${json.title},
completed:${json.completed}`)
  );

fetch("https://jsonplaceholder.typicode.com/todos", {
  // Sets the method of the "Request" object to "POST" following REST API
  // Default method is GET
  method: "POST",
  // Sets the header data of the "Request" object to be sent to the backend
  // Specified that the content will be in a JSON structure
  headers: {
    "Content-Type": "application/json",
  },
  // Sets the content/body data of the "Request" object to be sent to the backend
  // JSON.stringify converts the object data into a stringified JSON
  body: JSON.stringify({
    userId: 143,
    title: "to love somebody (pending)",
    completed: false,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    Title: "delectus aut autem",
    Description: "lorem ipsum",
    Status: "Pending",
    "Date Completed": "N/A",
    "User ID": 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/201", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    Title: "to love somebody (completed)",
    Status: "Completed",
    "Date Completed": "2022/12/17 03:24:00",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE",
});

/*
1. In the s33 folder, create an a1 folder and an index.html and a index.js
file inside of it.
2. Link the index.js file to the index.html file.
3. Create a fetch request using the GET method that will retrieve all the
to do list items from JSON Placeholder API.
4. Using the data retrieved, create an array using the map method to
return just the title of every item and print the result in the console.
5. Create a fetch request using the GET method that will retrieve a
single to do list item from JSON Placeholder API.

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

6

6. Using the data retrieved, print a message in the console that will
provide the title and status of the to do list item.
7. Create a fetch request using the POST method that will create a to
do list item using the JSON Placeholder API.
8. Create a fetch request using the PUT method that will update a to do
list item using the JSON Placeholder API.

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

7

9. Update a to do list item by changing the data structure to contain
the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID
10. Create a fetch request using the PATCH method that will update a to
do list item using the JSON Placeholder API.

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

8
11. Update a to do list item by changing the status to complete and add
a date when the status was changed.
12. Create a fetch request using the DELETE method that will delete an
item using the JSON Placeholder API.
13. Create a request via Postman to retrieve all the to do list items.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list items

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

9

14. Create a request via Postman to retrieve an individual to do list
item.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as get to do list item

15. Create a request via Postman to create a to do list item.
a. POST HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as create to do list item

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

10

16. Create a request via Postman to update a to do list item.
a. PUT HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as update to do list item PUT
d. Update the to do list item to mirror the data structure used in the PUT fetch
request

17. Create a request via Postman to update a to do list item.
a. PATCH HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as create to do list item
d. Update the to do list item to mirror the data structure of the PATCH fetch request

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

11

18. Create a request via Postman to delete a to do list item.
a. DELETE HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as delete to do list item

19. Export the Postman collection and save it in the activity folder.
20. Create a git repository named S33.
21. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
22. Add the link in Boodle.
*/

// ///////////////////////

// // [SECTION] JavaScript Synchronous vs Asynchronous
// // Javascript is by default is synchronous meaning that only one statement is executed at a time

// // Synchronous
// console.log("Hello World!");
// // conosle.log("Hello World!"); // will result to error
// console.log("Hello World!");

// // Asynchronous means that we can proceed to execute other statements, while consuming code is running in the background

// // [SECTION] Getting all posts
// // The Fetch API allows you to asynchronously request for a resource (data)
// // A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
// /*
// SYNTAX:
// 	fetch('URL')
// */

// // Fetch
// // "fetch()" method starts the process of fetching a resource from a server
// // "fetch()" method returns a promise that resolves to a Response object
// console.log(fetch("https://jsonplaceholder.typicode.com/posts/"));

// fetch("https://jsonplaceholder.typicode.com/posts/")
//   // ".then()" method captures the "response object" and returns another promise which will eventually be resolved or rejected
//   .then((response) => console.log(response.status));

// fetch("https://jsonplaceholder.typicode.com/posts/")
//   // Use the "json()" method from the "response object" to convert the data retrieved into JSON format to be used in our application
//   .then((response) => response.json())
//   // Print the converted JSON value from the "fetch" request
//   .then((json) => console.log(json));

// // Async and Await
// // The "async" and "await" keywords is another approach that can be used to achieve asynchronous code

// // Used in functions to indicate which portions of code should be waited for
// async function fetchData() {
//   // waits for the "fetch" method to complete then stores the value in the "result" variable
//   let result = await fetch("https://jsonplaceholder.typicode.com/posts/");
//   // Result returned by fetch is a returns a promise
//   console.log(result);
//   // The returned "Response" is an object
//   console.log(typeof result);
//   // We cannot access the content of the "Response" by directly accessing it's body property
//   console.log(result.body);

//   // Converts the data from the "Response" object as JSON
//   let json = await result.json();
//   // Print out the content of the "Response" object
//   console.log(json);
// }
// fetchData();

// // [SECTION] Creating a post
// /*
// SYNTAX:
// 	fetch('URL', options)
// 	.then((response)=>{})
// 	.then((response)=>{})
// */

// fetch("https://jsonplaceholder.typicode.com/posts/", {
//   // Sets the method of the "Request" object to "POST" following REST API
//   // Default method is GET
//   method: "POST",
//   // Sets the header data of the "Request" object to be sent to the backend
//   // Specified that the content will be in a JSON structure
//   headers: {
//     "Content-Type": "application/json",
//   },
//   // Sets the content/body data of the "Request" object to be sent to the backend
//   // JSON.stringify converts the object data into a stringified JSON
//   body: JSON.stringify({
//     title: "New Post",
//     body: "Hello world!",
//     userId: 1,
//   }),
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// // [SECTION] Updating a post
// // PUT - method used to update the whole document object
// // PATCH - method used to update a single/serveral properties

// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//   method: "PUT",
//   headers: {
//     "Content-Type": "application/json",
//   },
//   body: JSON.stringify({
//     userId: 2,
//     title: "Updated post",
//     body: "Hello again!",
//   }),
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// // [SECTION] Deleting a post
// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//   method: "DELETE",
// });
